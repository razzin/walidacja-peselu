package com.demo.springboot.rest;

public class Validate {
    public static boolean validate(String number) {
        int[] code={1,3,7,9,1,3,7,9,1,3,1};
        if(number.length()==11) {
            int[] intTab = number.chars().map(Character::getNumericValue).toArray();
            int check = 0;
            for(int i=0;i<intTab.length;i++){ check+=intTab[i]*code[i]; }
            return check % 10 == 0;
        }
        return false;
    }
}
